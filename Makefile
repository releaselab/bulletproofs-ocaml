# Invoke `make` to build, `make clean` to clean up, etc.

.PHONY: default build utop test clean install uninstall

default: build

build:
	dune build @install

install:
	opam pin add -n -y bulletproofs . 
	opam install -y bulletproofs

uninstall:
	opam pin remove -y bulletproof

deps:
	opam pin add -n -y bulletproofs .
	opam install -y bulletproofs --deps-only

# Launch utop such that it finds our library.
utop: build
	dune utop src/lib

# Clean up
clean:
# Remove files produced by dune.
	dune clean

# Build and run tests
test: deps build
	dune runtest
	dune clean

