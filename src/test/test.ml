open Bulletproofs.Generators
open Bulletproofs.Utils
open Bulletproofs.Rangeproofs
open Ecc.EccPrimitives
open Ecc.Secp256k1
open Printf

module Gens : Gens = struct 
  type t = {
    b : point;
    b_blinding : point;
    vec_G : point list; 
    vec_H : point list;
  }

  let (p, rem) = split Secp256k1.get_points 1
  let b_blinding = List.hd p 
  let (vec_G, rem) = split rem 64 
  let vec_H = rem

  let bp_gens = {
    b = Secp256k1.get_g;
    b_blinding = b_blinding;
    vec_G = vec_G;
    vec_H = vec_H;
  }

  let b = bp_gens.b 
  let b_blinding = bp_gens.b_blinding
  let vec_G = bp_gens.vec_H
  let vec_H = bp_gens.vec_H

end 

module BulletproofGens = Make_Gens (Secp256k1) (Gens)

module Rangeproof = Make_Rangeproof (BulletproofGens)

let single_proof() = 

  Random.self_init ();
  let value = Z.of_int (Random.int 10000) in 
  let blinding = random_big_int (BulletproofGens.get_n) in 
  let t = Sys.time() in 
  let (proof, value_commitment) = 
    Rangeproof.prove_single value blinding 64
  in 
  let time = truncate ((Sys.time() -. t) *. 1000.) in
  printf "Proving time: %dms\n" time;

  let t = Sys.time() in 
  assert(Rangeproof.verify_single proof value_commitment 64); 
  let time = truncate ((Sys.time() -. t) *. 1000.) in
  printf "Verification time: %dms\n" time

let multiple_proof_4() =

  Random.self_init ();
  let values = List.init 4 (fun _ -> Z.of_int (Random.int 10000)) in 
  let blindings = rand_numbers_list 4 BulletproofGens.get_n in 
  let t = Sys.time() in 
  let (proof, values_commitments) = 
    Rangeproof.prove_multiple values blindings 64
  in 
  let time = truncate ((Sys.time() -. t) *. 1000.) in
  printf "Proving time: %dms\n" time;
  let t = Sys.time() in  
  assert(Rangeproof.verify_multiple proof values_commitments 64); 
  let time = truncate ((Sys.time() -. t) *. 1000.) in
  printf "Verification time: %dms\n" time

let () = 
  printf "--------------------------TESTS --------------------------\n\n";

  printf "---> Aggregated 64-bit rangeproof creation/1\n";
  single_proof();
  print_newline();
  printf "---> Aggregated 64-bit rangeproof creation/4\n";
  multiple_proof_4();
  print_newline();
  printf "-----------------------END OF TESTS-----------------------\n\n";
