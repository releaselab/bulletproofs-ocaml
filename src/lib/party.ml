open Ecc.EccPrimitives
open Utils
open Messages
open Generators

module Make_Party (BpG : BulletproofGens) = struct 
  open BpG

  let compute_com_A v a_blinding gens = 
    (*Commitment of aL and aR*)
    let rec aux i acc g h =
      let bit = Z.( (v asr i ) land one) in 
      match g,h with 
      | [],[] -> acc
      | hg::tg,hh::th -> 
        if bit = Z.zero then
          aux (i+1) (add_point (inverse_point hh ) acc ) tg th 
        else
          aux (i+1) (add_point hg acc ) tg th 
      | _ -> 
        raise (Invalid_argument "The two lists have different lengths")
    in
    add_point (aux 0 Infinity gens.vec_G gens.vec_H)
      (multiply_point gens.b_blinding a_blinding) 

  let compute_com_S sL sR s_blinding gens = 
    (* Commitment of sL and SR *)
    multiscalar_mul ([s_blinding] @ sL @ sR) 
      ([gens.b_blinding] @ gens.vec_G @ gens.vec_H)


  let assign_position value blinding count gens value_commitments n =  

    (* Blinding factor of aL and aR*)
    let a_blinding = random_big_int gens.order in

    (* sL and sR *)
    let s_L = rand_numbers_list n gens.order in 
    let s_R = rand_numbers_list n gens.order in

    (* Blinding factor of sL and sR*)
    let s_blinding = random_big_int gens.order in

    (* Commitment of secret value v *)
    let com_v = 
      multiscalar_mul [value; blinding] 
        [gens.b; gens.b_blinding] 
    in
    value_commitments := !value_commitments @ [com_v];  

    (* Commitment of aL and aR *)
    let com_a = 
      compute_com_A value a_blinding gens
    in

    (* Commitment of sL and sR *)
    let com_s = 
      compute_com_S s_L s_R s_blinding gens
    in

    let j = !count in
    count := !count + 1;

    {
      j = j;
      v = value;
      v_blinding = blinding; 
      a_blinding = a_blinding;
      s_blinding = s_blinding;
      s_L = s_L;
      s_R = s_R;
      com_v = com_v; 
      com_a = com_a; 
      com_s = com_s
    }

  let compute_l_r_poly_vecs bit_com bit_chal l_p_vec r_p_vec = 
    (* Contruction of the polynomial vectors l and r *)
    let offset_y = Z.pow bit_chal.y (bit_com.j * bit_chal.n) in
    let offset_z = Z.pow bit_chal.z bit_com.j in 
    let zz = Z.(bit_chal.z * bit_chal.z) in 
    let exp_y = ref offset_y in 
    let exp_2 = ref Z.one in 
    let i = ref 0 in 

    List.iter2 (fun s_Li s_Ri -> 
        let aL_i = Z.( (bit_com.v asr !i ) land one) in 
        let aR_i = Z.(aL_i - Z.one) in

        l_p_vec.vec_poly1_0 <- 
          l_p_vec.vec_poly1_0 @ [Z.(aL_i - bit_chal.z)];

        l_p_vec.vec_poly1_1 <- 
          l_p_vec.vec_poly1_1 @ [s_Li];

        r_p_vec.vec_poly1_0 <-
          r_p_vec.vec_poly1_0 
          @ [Z.(!exp_y * (aR_i + bit_chal.z) + zz * offset_z * !exp_2)];

        r_p_vec.vec_poly1_1 <- 
          r_p_vec.vec_poly1_1 @ [Z.(!exp_y * s_Ri)];

        exp_y := Z.(!exp_y * bit_chal.y);
        exp_2 := Z.(!exp_2 * ~$2);
        i := !i + 1;
      ) bit_com.s_L bit_com.s_R

  let compute_t_poly l_p_vec r_p_vec =
    (* The inner_product of the above vector polynomials, using 
        the Karatsuba's method *)
    let t0 = inner_product l_p_vec.vec_poly1_0 r_p_vec.vec_poly1_0 in
    let t2 = inner_product l_p_vec.vec_poly1_1 r_p_vec.vec_poly1_1 in 

    let l0_plus_l1 = add_list l_p_vec.vec_poly1_0 l_p_vec.vec_poly1_1 in
    let r0_plus_r1 = add_list r_p_vec.vec_poly1_0 r_p_vec.vec_poly1_1 in

    let t1 = Z.((inner_product l0_plus_l1 r0_plus_r1) - t0 - t2) in 

    {poly2_0 = t0 ; poly2_1 = t1; poly2_2 = t2 }

  let compute_com_T1_T2 t_poly t1_blinding t2_blinding  =
    let com_t1 = multiscalar_mul [t_poly.poly2_1; t1_blinding]
        [get_b; get_b_blinding] in 
    let com_t2 = multiscalar_mul [t_poly.poly2_2; t2_blinding]
        [get_b; get_b_blinding] in 
    com_t1, com_t2 

  let apply_bit_challenge bit_com bit_chal =  

    let l_poly_vec = {vec_poly1_0 = []; vec_poly1_1 = []} in 
    let r_poly_vec = {vec_poly1_0 = []; vec_poly1_1 = []} in 
    compute_l_r_poly_vecs 
      bit_com bit_chal l_poly_vec r_poly_vec;

    let t_poly = compute_t_poly l_poly_vec r_poly_vec in 

    (* Blinding factors of t1 and t2 *)
    let t1_blinding = random_big_int get_n in 
    let t2_blinding = random_big_int get_n in 

    (*  Commitment of t1 and t2 *)
    let com_t1, com_t2 = 
      compute_com_T1_T2 t_poly t1_blinding t2_blinding 
    in 

    {
      bit_com = bit_com;
      offset_z = Z.pow bit_chal.z bit_com.j; 
      l_poly = l_poly_vec;
      r_poly = r_poly_vec;
      t_poly = t_poly;
      t1_blinding = t1_blinding;
      t2_blinding = t2_blinding;
      com_t1 = com_t1;
      com_t2 = com_t2
    }

  let apply_poly_challenge poly_comm poly_chal = 

    (* construct the polynomial of degree 2, the synthetic blinding factor of 
        t(x) and evaluate it at the point x *)
    let t_blinding_poly = 
      {
        poly2_0 = 
          Z.(poly_chal.bit_chal.z * poly_chal.bit_chal.z 
             * poly_comm.offset_z * poly_comm.bit_com.v_blinding); 
        poly2_1 = poly_comm.t1_blinding; 
        poly2_2 = poly_comm.t2_blinding } in 
    let t_x_blinding = 
      eval_poly2 t_blinding_poly poly_chal.x 
    in 

    (* evaluate the polynomial t(x) at the point x *)
    let t_x = 
      eval_poly2 poly_comm.t_poly poly_chal.x 
    in 

    (* compute the synthetic blinding factor of t(x) *)
    let e_blinding = 
      Z.(poly_comm.bit_com.a_blinding 
         + poly_comm.bit_com.s_blinding * poly_chal.x) 
    in 

    (* evaluate the polynomial vectors l(x) and r(x) at the point x *)
    let l_vec_x = 
      eval_vec_poly1 poly_comm.l_poly poly_chal.x 
    in 
    let r_vec_x = 
      eval_vec_poly1 poly_comm.r_poly poly_chal.x 
    in 

    {
      poly_com = poly_comm;
      t_x_blinding = t_x_blinding;
      t_x = t_x;
      e_blinding = e_blinding;
      l_vec = l_vec_x;
      r_vec = r_vec_x;
    }


end 