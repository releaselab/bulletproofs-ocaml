open Ecc.EccPrimitives
open Generators
open Utils
open Party
open Dealer 
open Inner_product_proof 
open Messages

module Make_Rangeproof (BpG : BulletproofGens) = 
struct

  open BpG 

  module Party = Make_Party (BpG)
  module Dealer = Make_Dealer (BpG)
  module Ipp_proof = Make_Ipp_proof (BpG)

  (* Create a rangeproof for a set of values. *)
  let prove_multiple (values : Z.t list) (blindings : Z.t list) (n : int) = 

    if List.length values <> List.length blindings then 
      raise (Error "Wrong number of blinding factors!");
    if not (n = 8 || n = 16 || n = 32 || n = 64) then 
      raise (Error "Invalid bit size!");
    if not (is_power_of_2 (List.length values)) then 
      raise (Error "Invalid proof aggregation!");
    if List.length get_vec_G = List.length get_vec_H 
    && List.length get_vec_G < n then 
      raise (Error "Invalid generators length!");

    let m = List.length values in 

    let gens = {
      b = get_b;
      b_blinding = get_b_blinding;
      vec_G = get_vec_G_n n;
      vec_H = get_vec_H_n n;
      order = get_n
    } in 

    (****************************BitCommitment*******************************)

    let value_commitments = ref [] in 
    let count = ref 0 in 

    let bit_com_var = List.map2 (fun v b ->
        Party.assign_position v b count gens value_commitments n
      ) values blindings
    in 

    (****************************BitChallenge*******************************)

    let bit_chal_var = 
      Dealer.receive_bit_commitments bit_com_var n m 
    in 

    (****************************PolyCommitment*******************************)

    let poly_com_var = List.map (fun var -> 
        Party.apply_bit_challenge var bit_chal_var
      ) bit_com_var 
    in 

    (****************************PolyChallenge*******************************)

    let poly_chal_var = 
      Dealer.receive_poly_commitments poly_com_var bit_chal_var 
    in 

    (****************************ProofShare*******************************)

    let proof_share_var = List.map (fun var -> 
        Party.apply_poly_challenge var poly_chal_var
      ) poly_com_var 
    in

    let proof = 
      Dealer.receive_trusted_shares proof_share_var poly_chal_var gens
    in 

    (proof, !value_commitments)

  (* Create a rangeproof of size n for a given pair of value 'v' 
      and blinding scalar 'v_blinding'. *)
  let prove_single (v : Z.t) (blinding : Z.t) (n : int) =
    let (proof, value_commitment) = 
      prove_multiple [v] [blinding] n 
    in 
    (proof, List.hd value_commitment)

  let delta n m y z =
    let sum_y = sum_of_powers (n * m) y  in
    let sum_2 = sum_of_powers n Z.(~$ 2) in
    let sum_z = sum_of_powers m z in 
    Z.( (z - z * z) * sum_y - z * z * z * sum_2 * sum_z) 


  (* Verifies an aggregated rangeproof for the given value commitments. *)
  let verify_multiple 
      (proof : range_proof) (value_commitments : point list)  (n : int) = 

    if not (n = 8 || n = 16 || n = 32 || n = 64) then 
      raise (Error "Invalid bit size!");
    if List.length get_vec_G = List.length get_vec_H 
    && List.length get_vec_G < n then 
      raise (Error "Invalid generators length!");

    let m = List.length value_commitments in

    let gens = {
      b = get_b;
      b_blinding = get_b_blinding;
      vec_G = get_vec_G_n n;
      vec_H = get_vec_H_n n;
      order = get_n
    } in 

    let s = List.fold_left (fun acc com_v -> 
        acc ^ point_to_string com_v) "" value_commitments 
    in 

    (* Obtain bitchallenges y and z through Fiat–Shamir heuristic *)
    let y = 
      let s = 
        s ^ point_to_string proof.aA_ ^ point_to_string proof.sS_ 
      in
      return_challenge s
    in 
    let z = return_challenge (Z.to_string y) in 
    let zz = Z.(z * z) in
    let minus_z = Z.(-z) in

    let x = 
      let s = 
        point_to_string proof.tT1_ ^ point_to_string proof.tT2_ 
      in
      return_challenge s  
    in

    let w =  
      let s = Z.to_string proof.t_x_ ^ 
              Z.to_string proof.t_x_blinding_ ^ 
              Z.to_string proof.e_blinding_ 
      in
      return_challenge s 
    in 

    let c = random_big_int gens.order in

    let u_sq, u_inv_sq, s = 
      Ipp_proof.verification_scalars (n * m) proof.ipp_proof 
    in

    let a0 = proof.ipp_proof.a0 in 
    let b0 = proof.ipp_proof.b0 in 

    let g_scalar = 
      List.map (fun s_i -> 
          Z.(minus_z - a0 * s_i )) s 
    in

    let h_scalar = 
      let rec aux i y_inv exp_y_inv exp_z exp_2 acc = function
        | [] -> List.rev acc
        | h::t when i = n - 1 -> 
          aux 0 y_inv Z.(y_inv * exp_y_inv) Z.(exp_z * z) Z.one  
            (Z.(z + exp_y_inv * (zz * exp_z * exp_2 - b0 * h)) 
             :: acc) t 
        | h::t -> 
          aux (i + 1) y_inv Z.(y_inv * exp_y_inv) exp_z Z.(~$2 * exp_2) 
            (Z.(z + exp_y_inv * (zz * exp_z * exp_2 - b0 * h)) 
             :: acc) t 
      in
      aux 0 (inverse y gens.order) Z.one Z.one Z.one [] (List.rev s) 
    in

    let value_commitment_scalar =
      let rec aux m z_exp acc = 
        if m = 0 then 
          List.rev acc 
        else
          aux (m - 1) Z.(z * z_exp) (Z.(c * zz * z_exp) :: acc) 
      in 
      aux m Z.one []
    in 

    let basepoint_scalar = 
      Z.(w * (proof.t_x_ - a0 * b0) + c * ((delta n m y z) - proof.t_x_))
    in

    let check = multiscalar_mul 
        ([Z.one; 
          x; 
          Z.(c * x); 
          Z.(c * x * x); 
          basepoint_scalar;
          Z.(- proof.e_blinding_ - c * proof.t_x_blinding_)] 
         @ g_scalar
         @ h_scalar
         @ u_sq
         @ u_inv_sq
         @ value_commitment_scalar)

        ([proof.aA_; 
          proof.sS_; 
          proof.tT1_;
          proof.tT2_;
          gens.b;
          gens.b_blinding]
         @ (concat_m_times gens.vec_G m)
         @ (concat_m_times gens.vec_H m)
         @ proof.ipp_proof.lL_vec
         @ proof.ipp_proof.rR_vec
         @ value_commitments) 
    in 

    if check = Infinity then 
      true
    else 
      false

  (* Verifies a rangeproof for a unique value commitment *)  
  let verify_single 
      (proof : range_proof) (value_commitment : point) (n : int) = 
    verify_multiple proof [value_commitment] n 

end 