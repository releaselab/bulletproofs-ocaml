open Ecc.EccPrimitives
open Utils
open Inner_product_proof

type bit_commitment = {
  j : int;
  v :  Z.t;
  v_blinding : Z.t;
  a_blinding : Z.t;
  s_blinding : Z.t;
  s_L : Z.t list;
  s_R : Z.t list; 
  com_v : point; 
  com_a : point;
  com_s : point
}

type bit_challenge = {
  n : int;
  m : int;
  y : Z.t;
  z : Z.t;
  aA : point;
  sS : point;
}

type poly_commitment = {
  bit_com : bit_commitment;
  offset_z : Z.t; 
  l_poly : vec_poly1;
  r_poly : vec_poly1;
  t_poly : poly2;
  t1_blinding : Z.t;
  t2_blinding : Z.t;
  com_t1 : point;
  com_t2 : point 
}

type poly_challenge = {
  bit_chal : bit_challenge;
  x : Z.t;
  tT1 : point;
  tT2 :point; 
}

type proof_share = {
  poly_com : poly_commitment;
  t_x_blinding : Z.t;
  t_x : Z.t;
  e_blinding : Z.t;
  l_vec : Z.t list;
  r_vec : Z.t list;
}

type range_proof = {
  aA_ : point; 
  sS_ : point;
  tT1_ : point;
  tT2_ : point;
  t_x_ : Z.t;
  t_x_blinding_ : Z.t;
  e_blinding_ : Z.t;
  ipp_proof : inner_product_proof
}
(** Type with the elements generated during the proof, to be used to verify the proof. *)