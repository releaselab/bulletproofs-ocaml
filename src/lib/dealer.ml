(** Private module *)

open Ecc.EccPrimitives
open Messages 
open Utils
open Generators
open Inner_product_proof

module Make_Dealer (BpG : BulletproofGens) = struct
  open BpG 
  module Ipp_proof = Make_Ipp_proof (BpG)

  let receive_bit_commitments bit_com n m =
    (* Concatenation of all commitment of v, sent for each party *)
    let s = List.fold_left (fun acc var -> 
        acc ^ point_to_string var.com_v) "" bit_com
    in 

    (* Sum of all commitments a and s, sent by each party *)
    let aA = List.fold_left (fun acc var ->
        add_point acc var.com_a) Infinity bit_com
    in 
    let sS = List.fold_left (fun acc var ->
        add_point acc var.com_s) Infinity bit_com
    in 

    (* Obtain bitchallenges y and z through Fiat–Shamir heuristic *)
    let y = 
      let s = s ^ point_to_string aA ^ point_to_string sS in
      return_challenge s
    in 
    let z = return_challenge (Z.to_string y) in 

    {
      n = n;
      m = m;
      y = y; 
      z = z;
      aA = aA;
      sS = sS;
    } 

  let receive_poly_commitments poly_com bit_chal = 
    (* Sum of all commitments t1 and t2, sent by each party *)
    let tT1 = List.fold_left (fun acc var ->
        add_point acc var.com_t1) Infinity poly_com
    in 
    let tT2 = List.fold_left (fun acc var ->
        add_point acc var.com_t2) Infinity poly_com
    in 

    (* Obtain poychallenge x through Fiat–Shamir heuristic *)
    let x = 
      let s = point_to_string tT1 ^ point_to_string tT2 in
      return_challenge s  
    in

    {
      bit_chal = bit_chal;
      x = x;
      tT1 = tT1;
      tT2 = tT2; 
    }

  let receive_trusted_shares proof_shares poly_chal gens  = 
    if poly_chal.bit_chal.m <> List.length proof_shares then 
      raise (Error "Wrong number of Proof shares");

    (* Sum of all t_x t_x_blinding and e_blinding, sent by each party *)
    let t_x = List.fold_left (fun acc var ->
        Z.(acc + var.t_x)) Z.zero proof_shares
    in 
    let t_x_blinding = List.fold_left (fun acc var ->
        Z.(acc + var.t_x_blinding)) Z.zero proof_shares
    in 
    let e_blinding = List.fold_left (fun acc var ->
        Z.(acc + var.e_blinding)) Z.zero proof_shares
    in 

    (* Obtain a challenge value to combine statements for the inner_product_proof *)
    let w =  
      let s = Z.to_string t_x ^ 
              Z.to_string t_x_blinding ^ 
              Z.to_string e_blinding in
      return_challenge s 
    in 
    let qQ = multiply_point gens.b w in

    let pow_y_inv = 
      Ipp_proof.pow_y_inv (poly_chal.bit_chal.n * poly_chal.bit_chal.m) 
        poly_chal.bit_chal.y 
    in 

    let l_vec = List.fold_left (fun acc var -> 
        acc @ var.l_vec) [] proof_shares 
    in 
    let r_vec = List.fold_left (fun acc var -> 
        acc @ var.r_vec) [] proof_shares 
    in 

    let vec_G = 
      concat_m_times gens.vec_G poly_chal.bit_chal.m 
    in 
    let vec_H = 
      concat_m_times gens.vec_H poly_chal.bit_chal.m 
    in


    let ipp_proof = 
      Ipp_proof.create_ipp_proof qQ pow_y_inv vec_G vec_H 
        l_vec r_vec 
    in 

    {
      aA_ = poly_chal.bit_chal.aA; 
      sS_ = poly_chal.bit_chal.sS;
      tT1_ = poly_chal.tT1;
      tT2_ = poly_chal.tT2;
      t_x_ = t_x;
      t_x_blinding_ = t_x_blinding;
      e_blinding_ = e_blinding;
      ipp_proof = ipp_proof
    }


end 