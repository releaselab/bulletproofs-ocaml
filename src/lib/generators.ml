open Ecc.EccPrimitives
open Utils

module type Gens =
sig

  type t

  val bp_gens : t 

  val b : point 

  val b_blinding : point 

  val vec_G : point list 

  val vec_H : point list 

end

module type BulletproofGens =
sig

  val get_b : point 
  val get_b_blinding : point 
  val get_vec_G : point list 
  val get_vec_H : point list 
  val get_vec_G_n : int -> point list 
  val get_vec_H_n : int -> point list 
  val get_n : Z.t
  val add_point : point -> point -> point 
  val inverse_point : point -> point 
  val multiply_point : point -> Z.t -> point 
  val multiscalar_mul : Z.t list -> point list -> point 

end

type gens = {
  b : point;
  b_blinding : point;
  vec_G : point list;
  vec_H : point list;
  order : Z.t;
}

module Make_Gens (C : Curve) (G : Gens)
  : BulletproofGens = struct

  open C
  open G

  let get_b = b
  let get_b_blinding = b_blinding
  let get_vec_G = vec_G
  let get_vec_H = vec_H
  let get_vec_G_n x = 
    let (vec_G, _ ) = split vec_G x in 
    vec_G
  let get_vec_H_n x = 
    let (vec_H, _ ) = split vec_H x in 
    vec_H

  let get_n = get_n

  let add_point = add_point
  let inverse_point = inverse_point 
  let multiply_point = multiply_point
  let multiscalar_mul = multiscalar_mul Infinity

end 
