open Ecc.EccPrimitives

module type Gens = sig
  type t
  (** type of [Gens] with all the generators. *)

  val bp_gens : t
  (** A variable of type [t]. *)

  val b : point
  (** Variable with the base point for the value. *)

  val b_blinding : point
  (** Variable with the base point for the blinding factors. *)

  val vec_G : point list
  (** Variable with a list of points for the [vector_G]. *)

  val vec_H : point list
  (** Variable with a list of points for the [vector_H]. *)

end

module type BulletproofGens =
sig

  val get_b : point
  (** Returns the base point for the value. *)

  val get_b_blinding : point
  (** Returns the base point for the blinding factors. *)

  val get_vec_G : point list
  (** Returns the [vector_G], a list of points. *)

  val get_vec_H : point list
  (** Returns the [vector_H], a list of points. *)

  val get_vec_G_n : int -> point list
  (** [BulletproofGens.get_vec_G_n x] Returns the [vec_G] split on [x]. *)

  val get_vec_H_n : int -> point list
  (** [BulletproofGens.get_vec_H_n x] Returns the [vec_H] split on [x]. *)

  val get_n : Z.t
  (** Returns the order of the curve points. *)

  val add_point : point -> point -> point
  (** [BulletproofGens.add_point p1 p2] Returns the addition of two points, [p1] + [p2]. *)

  val inverse_point : point -> point
  (** [BulletproofGens.inverse_point p] Returns the inverse point of [p], [-p]. *)

  val multiply_point : point -> Z.t -> point
  (** [BulletproofGens.multiply_point p s] Returns the multiplication of a scalar 
       [s] with a poin [p]. *)

  val multiscalar_mul : Z.t list -> point list -> point
  (** [BulletproofGens.multiscalar_mul s_list p_list] Returns the multiscalar 
      multiplication of scalars [s_list] with points [p_list]. *)

end

type gens = {
  b : point;
  b_blinding : point;
  vec_G : point list;
  vec_H : point list;
  order : Z.t;
}
(** Type with the generators to be used during the proof. *)

module Make_Gens :
  functor (C : Curve) (G : Gens) -> BulletproofGens


