open Ecc.EccPrimitives
open Utils
open Generators 

type inner_product_proof = {
  lL_vec : point list;
  rR_vec : point list;
  a0 : Z.t;
  b0 : Z.t 
}

module Make_Ipp_proof (BpG : BulletproofGens) = struct 
  open BpG

  let pow_y_inv n y = 
    let rec aux n y_inv exp_y_inv acc = 
      if n = 0 then 
        List.rev acc
      else  
        aux (n - 1) y_inv Z.(exp_y_inv * y_inv) 
          (exp_y_inv :: acc) 
    in
    aux n (inverse y get_n) Z.one []

  let create_ipp_proof qQ pow_y_inv vec_G vec_H l_vec r_vec =
    let rec aux p_size i a b g h pow_y_inv l_vec r_vec = 
      if i = 1 then
        {
          lL_vec = List.rev l_vec;
          rR_vec = List.rev r_vec;
          a0 = List.hd a;
          b0 = List.hd b;
        }
      else if i = p_size then 
        begin
          let (a_l, a_r) = split a (i/2) in
          let (b_l, b_r) = split b (i/2) in 
          let (g_l, g_r) = split g (i/2) in
          let (h_l, h_r) = split h (i/2) in
          let (p_y_inv_l, p_y_inv_r) = 
            split pow_y_inv (i/2) in

          let c_l = inner_product a_l b_r in
          let c_r = inner_product a_r b_l in 

          let l = multiscalar_mul
              (a_l @ (List.map2 (fun a b -> Z.(a * b)) b_r p_y_inv_l) 
               @ [c_l]) 
              (g_r @ h_l @ [qQ])
          in

          let r = multiscalar_mul
              (a_r @ (List.map2 (fun a b -> Z.(a * b)) b_l p_y_inv_r)  
               @ [c_r]) (g_l @ h_r @ [qQ])
          in 

          let u = 
            let s = point_to_string l ^ point_to_string r in
            return_challenge s 
          in 

          let u_inv = inverse u get_n in 

          let a_ = List.map2 (fun a b -> Z.(a * u + u_inv * b)) 
              a_l a_r
          in
          let b_  = List.map2 (fun a b -> Z.(a * u_inv + u * b)) 
              b_l b_r
          in 

          let g_ = List.map2 (fun a b -> 
              multiscalar_mul [u_inv; u] [a; b])
              g_l g_r 
          in  

          let h_ = 
            List.map2 (fun a b -> add_point a b )
              (List.map2 (fun a b -> 
                   multiscalar_mul [Z.(a * u)] [b]) 
                  p_y_inv_l h_l)
              (List.map2 (fun a b-> 
                   multiscalar_mul [Z.(a * u_inv)] [b])
                  p_y_inv_r h_r)
          in

          aux p_size (i/2) a_ b_ g_ h_ pow_y_inv (l :: l_vec) (r :: r_vec) 
        end
      else
        begin
          let (a_l, a_r) = split a (i/2) in
          let (b_l, b_r) = split b (i/2) in 
          let (g_l, g_r) = split g (i/2) in
          let (h_l, h_r) = split h (i/2) in
          let c_l = inner_product a_l b_r in
          let c_r = inner_product a_r b_l in 

          let l = multiscalar_mul 
              (a_l @ b_r @ [c_l]) (g_r @ h_l @ [qQ])
          in

          let r = multiscalar_mul
              (a_r @ b_l @ [c_r]) (g_l @ h_r @ [qQ])
          in 

          let u = 
            let s = point_to_string l ^ point_to_string r in
            return_challenge s
          in 

          let u_inv = inverse u get_n in 

          let a_ = List.map2 (fun a b -> Z.(a * u + u_inv * b)) 
              a_l a_r
          in
          let b_  = List.map2 (fun a b -> Z.(a * u_inv + u * b)) 
              b_l b_r
          in 

          let g_ = List.map2 (fun a b -> 
              multiscalar_mul [u_inv; u] [a; b])
              g_l g_r 
          in  
          let h_ = List.map2 (fun a b -> 
              multiscalar_mul [u; u_inv] [a; b])
              h_l h_r 
          in

          aux p_size (i/2) a_ b_ g_ h_ pow_y_inv (l :: l_vec) (r :: r_vec) 
        end
    in 
    let proof_size = List.length vec_G in

    (* All vector must have the same lenght *)
    assert(List.length vec_H = proof_size);
    assert(List.length l_vec = proof_size);
    assert(List.length r_vec = proof_size);
    (* And the length must be a power of two *)
    assert(is_power_of_2 proof_size);

    aux proof_size proof_size l_vec r_vec
      vec_G vec_H pow_y_inv [] []

  (* Computes three vectors of verification scalars (u^2, u^-2 and s) 
      for combined multiscalar multiplication *)
  let verification_scalars n ipp_proof =
    (* Computes the challenges u and return the lists with 
        u², u⁻² values and the value of multiplication of all u⁻¹ *)
    let rec aux_u acc acc_inv mul_allinv =
      function  
      | [], [] -> 
        List.rev acc, List.rev acc_inv, mul_allinv
      | h_l::t_l, h_r::t_r -> 
        let u = 
          let s = point_to_string h_l ^ point_to_string h_r in
          return_challenge s
        in 
        let u_inv = inverse u get_n in  
        aux_u (Z.(u * u) :: acc) (Z.(u_inv * u_inv) :: acc_inv) 
          Z.(mul_allinv * u_inv) (t_l,t_r)
      | _ -> 
        raise (Invalid_argument "The two lists have different lengths")
    in 
    (* Compute the s values inductively *)
    let rec aux_s i log2_n u_sq acc = 
      if i = n then
        Array.to_list acc
      else
        let log2_i = truncate (log (float i)/. log 2.) in
        let k = 1 lsl log2_i in 
        (* The challenges are stored in creation order as [u_k,...,u_1],
            so u[log2(i)+1] = is indexed by (log2_n-1) - log2_i *)
        let u_log2_i_sq = u_sq.((log2_n - 1) - log2_i) in
        acc.(i) <- Z.mul acc.(i - k) u_log2_i_sq;
        aux_s (i + 1) log2_n u_sq acc
    in 
    let log2_n = List.length ipp_proof.lL_vec in
    (* TO DO: check log2_n *)
    let u_sq, u_inv_sq, mul_allinv = 
      aux_u [] [] Z.one (ipp_proof.lL_vec, ipp_proof.rR_vec) 
    in
    let s_array = Array.make n Z.one in
    s_array.(0) <- mul_allinv;
    let s = aux_s 1 log2_n (Array.of_list u_sq) s_array in 
    u_sq, u_inv_sq, s

end 