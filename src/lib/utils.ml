open Ecc.EccPrimitives

exception Error of string 

type vec_poly1 = {
  mutable vec_poly1_0 : Z.t list;
  mutable vec_poly1_1 : Z.t list;
}

let eval_vec_poly1 p1 x = 
  List.map2 (fun a b ->  Z.(a + b * x) ) 
    p1.vec_poly1_0 p1.vec_poly1_1

type poly2 = {
  poly2_0 : Z.t;
  poly2_1 : Z.t;
  poly2_2 : Z.t;
}

let eval_poly2 p2 x = 
  Z.(p2.poly2_0 + x * (p2.poly2_1 + x * p2.poly2_2))

let split list x =
  let rec aux i acc = function
    | [] -> List.rev acc, []
    | h :: t as l -> if i = 0 then List.rev acc, l
      else aux (i-1) (h :: acc) t  in
  aux x [] list

let is_power_of_2 number = 
  number > 0 && 
  ((number land (number - 1)) == 0)

let rand_numbers_list tam bound = 
  List.init tam (fun _ -> random_big_int bound)

let return_challenge s = 
  let hash = Sha256.string s in
  let z_hash = Z.of_string ("0x"^(Sha256.to_hex hash)) in 
  z_hash

let inner_product = 
  List.fold_left2 (fun a b c -> 
      Z.(b * c + a)) Z.zero

let add_list = 
  List.map2 (fun a b -> Z.(a + b))

let concat_m_times vec x = 
  let rec aux vec m acc = 
    if m = 0 then 
      acc 
    else 
      aux vec (m-1) (acc @ vec)
  in 
  aux vec x []

let sum_of_powers_slow n x  =
  let rec aux n x_exp acc = 
    if n = 0 then acc 
    else
      aux (n-1) Z.(x * x_exp) Z.(acc + x_exp)
  in 
  aux n Z.one Z.zero

(** when n is power of two
    begin with acc = exp + 1 *)
let sum_of_powers n x = 
  let rec aux n x acc =
    if is_power_of_2 n then 
      if n = 0 || n = 1 then  
        Z.one
      else if n = 2 then
        acc 
      else
        let exp = Z.(x * x) in 
        aux (n/2) exp Z.(acc + exp * acc)
    else 
      sum_of_powers_slow n x  
  in 
  aux n x Z.(x + one)