exception Error of string
(** Exception of Error with the message of error of type string *)

type vec_poly1 = {
  mutable vec_poly1_0 : Z.t list;
  mutable vec_poly1_1 : Z.t list;
}
(** Type of a degree-1 vector polynomial with the fields:

    [vec_poly1_0] A list with the elements of degree-0

    [vec_poly1_1] A list with the elements of degree-1 *)

val eval_vec_poly1 : vec_poly1 -> Z.t -> Z.t list
(** [Utils.eval_vec_poly1 p1 x] Evaluates the degree-1 vector polynomial p1 on x *)

type poly2 = { poly2_0 : Z.t; poly2_1 : Z.t; poly2_2 : Z.t; }
(** Type of a degree-2 scalar polynomial with the fields: 

    [poly2_0] A scalar of degree-0;

    [poly2_1] A scalar of degree-1; 

    [poly2_2] A scalar of degree-2. *)

val eval_poly2 : poly2 -> Z.t -> Z.t
(** [Utils.eval_poly2 p1 x] Evaluates the degree-1 vector polynomial [p1] on [x]. *)

val split : 'a list -> int -> 'a list * 'a list
(** [Utils.split list x] Slits the [list] on [x] and returns and returns a pair of the 
    two originated lists. *)

val is_power_of_2 : int -> bool 
(** Returns if a number is a power of two or not. *)

val rand_numbers_list : int -> Z.t -> Z.t list
(** [Utils.rand_numbers_list tam bound] Returns a list of random numbers between [0 
    (inclusive)] and [bound] with lenght [tam]. *)

val return_challenge : string -> Z.t
(** Returns the challenge of a given input. *)

val inner_product : Z.t list -> Z.t list -> Z.t
(** Returns the inner product of two lists. *)

val add_list : Z.t list -> Z.t list -> Z.t list
(** Returns the sum of two lists. *)

val concat_m_times : 'a list -> int -> 'a list 
(** [Utils.concat_m_times vec x] Returns the list with the concatonation of 
    [vec] [x] times. *)

val sum_of_powers : int -> Z.t -> Z.t
(** [Utils.sum_of_powers n x] Returns the sum of powers of [x] between [0] and [n]. *)