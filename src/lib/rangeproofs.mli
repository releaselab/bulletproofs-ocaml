open Generators
open Messages
open Ecc.EccPrimitives


module Make_Rangeproof :
  functor (BpG : BulletproofGens) ->
  sig

    val prove_multiple : Z.t list -> Z.t list -> int -> range_proof * point list
    (** [Make_Rangeproof.prove_multiple values blindings n] Creates a  aggregated [n] bit size rangeproof, where
        [n] must be a power of two, [values] is a list of secret values and [blindings] is a list of random scalars 
        for each value, these two list must have the same lenght and must be a power of two. Returns a [rangeproof] 
        and a list with all the [commitments] for each secret value. *)

    val prove_single : Z.t -> Z.t -> int -> range_proof * point
    (** [Make_Rangeproof.prove_single value blinding n] Creates a single [n] bit size rangeproof, where
        [n] must be a power of two, [value] is a scret value and [blinding] is a random scalar. Returns a [rangeproof]
        and the [commitment] for the secret value.*)

    val verify_multiple : range_proof -> point list -> int -> bool
    (** [Make_Rangeproof.verify_multiple range_proof commitments n] Verifies if the aggregated [n] bit size [rangeproof]
        it's correct or not, where the [commitments] is a list of all commitments for each secret value. *)

    val verify_single : range_proof -> point -> int -> bool
    (** [Make_Rangeproof.verify_single range_proof commitments n] Verifies if the single [n] bit size [rangeproof]
        it's correct or not, where the [commitment] is a  commitment for the secret value. *)

  end