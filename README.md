# Bulletproofs-Ocaml

Bulletproofs are short non-interactive zero-knowledge proofs that require no trusted setup. A bulletproof can be used to convince a verifier that an encrypted plaintext is well formed. For example, prove that an encrypted number is in a given range, without revealing anything else about the number. 

This project is inspired on [project implemented in Rust by Chain, Inc](https://github.com/dalek-cryptography/bulletproofs). For them, many thanks for the great documentation and work.

For more information about Bulletproofs you can use their official site [here](https://crypto.stanford.edu/bulletproofs/).

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

### Prerequisites

The project depends on the following dependencies, but you don't need to install them, the installation command install the dependencies for you. The only software you need to install is the compiler OCaml and the package manager OPAM.

* [OCaml](https://ocaml.org/docs/install.html) - The compiler version 4.02.3 or newer
* [OPAM](https://opam.ocaml.org/doc/Install.html) - The OCaml package manager
* [ECC-OCaml](https://github.com/jcfpascoal10/ECC-OCaml) - Elliptic Curves Cryptography for OCaml, with the basic arithmetic operations 
* [Zarith](https://opam.ocaml.org/packages/zarith/) - OCaml Library  for arithmetic and logical operations over arbitrary-precision integers
* [SHA](https://opam.ocaml.org/packages/sha/) - OCaml Library  with SHA cryptographic functions
* [Dune](https://opam.ocaml.org/packages/dune/) - Ocaml Package for compiling the project


### Installing

For install you need to run this command on terminal:

```
$ opam pin add -n bulletproofs https://gitlab.com/releaselab/bulletproofs-ocaml.git
$ opam install bulletproofs
```

### Example

The example below uses the Secp256k1 curve defined in the [Secp256k1 module](https://github.com/jcfpascoal10/ECC-OCaml/blob/master/src/Secp256k1.ml) of the Ecc package, but you can define any curve with equation ![ecc equation](http://latex.codecogs.com/gif.latex?y%5E%7B2%7D%20%3D%20x%5E%7B3%7D%20&plus;%20ax%20&plus;b). You need to fill the generators with points or you can use the points on Secp256k1 module, like in example below:

```ocaml
open Bulletproofs.Generators
open Bulletproofs.Utils
open Bulletproofs.Rangeproofs
open Ecc.EccPrimitives
open Ecc.Secp256k1
open Printf

module Gens : Gens = struct 
  type t = {
    b : point;
    b_blinding : point;
    vec_G : point list; 
    vec_H : point list;
  }

  let (p, rem) = split Secp256k1.get_points 1
  let b_blinding = List.hd p 
  let (vec_G, rem) = split rem 64 
  let vec_H = rem

  let bp_gens = {
    b = Secp256k1.get_g;
    b_blinding = b_blinding;
    vec_G = vec_G;
    vec_H = vec_H;
  }

  let b = bp_gens.b 
  let b_blinding = bp_gens.b_blinding
  let vec_G = bp_gens.vec_H
  let vec_H = bp_gens.vec_H

end 

module BulletproofGens = Make_Gens (Secp256k1) (Gens)

module Rangeproof = Make_Rangeproof (BulletproofGens)

let value = Z.of_int 5

let blinding = random_big_int (BulletproofGens.get_n)

let t = Sys.time() 

let (proof, value_commitment) = 
  Rangeproof.prove_single value blinding 64

let () =
  let time = truncate ((Sys.time() -. t) *. 1000.) in
  printf "Proving time: %dms\n" time

let t = Sys.time() 

let () = 
  assert(Rangeproof.verify_single proof value_commitment 64); 

  let time = truncate ((Sys.time() -. t) *. 1000.) in
  printf "Verification time: %dms\n" time

```

The example above is a simple rangeproof, but you can create a aggregated rangeproof with multiple values.
For more information about the bulletproofs package you can consult the [package documentation](https://releaselab.gitlab.io/bulletproofs-ocaml/).

### Running the tests

For run tests, you can install the package and, compile and run the above example, or make your own test. 

You can also download the repository, by run this command in terminal to clone the repository: 

```
$ git clone https://gitlab.com/releaselab/bulletproofs-ocaml.git
  
```

and run the tests with the Makefile in the following way:

```
$ cd bulletproofs-ocaml
$ make test
```
NOTE: This command only installs the dependencies and not the package. 

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/releaselab/bulletproofs-ocaml/tags). 

## Authors

* **Jose Pascoal** - *Main author and maintainer* - [jcfpascoal](https://gitlab.com/jcfpascoal)
* **Simao Melo de Sousa** - [smdsousa](https://gitlab.com/smdsousa)

## License

This project is licensed under the MIT License - see the [LICENSE.md](https://gitlab.com/releaselab/bulletproofs-ocaml/blob/master/LICENSE) file for details
